var chai = require('chai');
var expect = chai.expect;

var register = require('../src/register')

describe('Register', function () {
    it('Should print out the name used to register', function () {
        expect(register.getName('John')).to.equal('John');
    })
})